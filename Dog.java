public class Dog{
    private String name;
    private int energy;
    private int hunger;
    private int boredom; 

    public Dog(String name){
        this.name = name;
        this.energy = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    //method to check the dog's mood
    public boolean takeNap(){
        if(this.hunger > 50 && this.boredom > 50){
            System.out.println("The dog was either too bored or too hungry to nap ");
            return false;
        }else{
            this.energy += 20;
            this.hunger += 5;
            this.boredom +=10;
            System.out.println("The dog napped successfully ");
            return true;
        }
    }

    //Method to eat
    public void eat(){
        this.hunger = this.hunger - 30;

        //Check if hunger is below 0 and correct
        if(this.hunger < 0){
            this.hunger = 0;
        }
    }

    //Method reduces the bordom and increases hunger
    public void play(){
        this.boredom = this.boredom - 30;
        this.hunger += 5;
        System.out.println("The dog played with the human.");
    }

    //Method to reduce dog energy
    public boolean playWith(Human human){
        if(this.energy < 50){
            System.out.println("that the dog doesn’t have enough energy to play");
            return false;
        }else{
            this.energy = this.energy - 50;
            
            human.play();
            return true;
        }
    }

}