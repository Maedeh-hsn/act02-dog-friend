public class Human{
    //Field initialization
    private String name;
    private int money;
    private int hunger;
    private int boredom;

    //Constructor
    public Human(String name, int money, int hunger, int boredom){
        this.name = name;
        this.money = money;
        this.hunger = hunger;
        this.boredom = boredom;
    }

    //Method to do work
    public boolean doWork(){
        //Unsuccessful work
        if(this.hunger > 50){
            System.out.println(this.name + " is too hungry to work.");
            return false;
        }
        else if(this.boredom > 50){
            System.out.println(this.name + " is too bored to work.");
            return false;
        }
        else if(this.hunger > 50 && this.boredom > 50){
            System.out.println(this.name + " is too hungry and bored to work.");
            return false;
        }

        //Successful work
        else{
            this.money = this.money + 5;
            this.hunger = this.hunger + 5;
            this.boredom = this.boredom + 10;

            System.out.println(this.name + " completed their work successfully!");
            return true;
        }
    }

    //Method to eat
    public void eat(){
        this.hunger = this.hunger - 30;

        //Check if hunger is below 0 and correct
        if(this.hunger < 0){
            this.hunger = 0;
        }
    }

    //Method to feed dog
    public boolean feed(Dog dog, Human human){
        //Can afford food
        if(this.money >= 50){
            this.money = this.money - 50;

            human.eat();
            dog.eat();

            System.out.println(this.name + " bought food for everyone.");
            return true;
        }

        //Cannot afford food
        else{
            System.out.println(this.name + " cannot afford food right now.");
            return false;
        }
    }
    //Method to reduce the human energy
    public void play(){
        this.boredom = this.boredom - 30;
        this.hunger += 5;
        System.out.println("The human played with the dog.");
    }
}